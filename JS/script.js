"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------
// ------< TASK#1 >------------------------------

// Код для заданий находится в папке PROJECT (добавлен в папку с ДЗ)
// Найти все параграфы на странице и установить цвет фона #ff0000
// Найти элемент с id="optionsList". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды (если они есть) и вывести в консоль названия и тип нод.
// Устновить в качестве контента элемента с классом testParagraph следующий параграф: "This is a paragraph".
// Получить элементы, вложенные в элемент с классом main-header и вывести их в консоль.
// Каждому элементу присвоить новый класс nav-item.
// Найти все элементы с классом section-title. Удалить этот класс у данных элементов.

// Через консоль каждый пункт задачи отделен - для удобства 


//  Start ot the TASK#1 ------------------------------------------------------------------------------
//  ХОД ВЫПОЛНЕНИЯ ЗАДАЧИ: 
console.log ('Task #1 < HW8 - searching-dom-tasks > started  \u{1F680}');

//  Найти все параграфы на странице и установить цвет фона #ff0000:
console.log(`\u{1F6A9} Найти все параграфы на странице и установить цвет фона #ff0000:`);

const allParagraphs = document.querySelectorAll('p');
console.log(allParagraphs);

for (const p of allParagraphs) {
    p.style.backgroundColor = '#ff0000';
}

// Найти элемент с id="optionsList". Вывести в консоль.
console.log(`\u{1F6A9} Найти элемент с id="optionsList". Вывести в консоль:`);

const optionsListElement = document.getElementById('optionsList');
console.log(optionsListElement);

// Найти родительский элемент и вывести в консоль.
console.log(`\u{1F6A9} Найти родительский элемент и вывести в консоль:`);

 const paretnElement = optionsListElement.parentElement;
 console.log(paretnElement);

// Найти дочерние ноды (если они есть) и вывести в консоль названия и тип нод.
console.log(`\u{1F6A9} Найти дочерние ноды (если они есть) и вывести в консоль названия и тип нод: 1 - элемент / 3 - текст / 2 - атрибут / 8 - комментарий: `);

const childNodes = optionsListElement.childNodes;
console.log(childNodes);

for (let node = 0; node < childNodes.length; node++) {
    const childNode = childNodes[node];
    console.log(`NodeName: ${childNode.nodeName}, NodeType: ${childNode.nodeType}`);
}

// Установить в качестве контента элемента с классом testParagraph следующий параграф: "This is a paragraph".

console.log(`\u{1F6A9} Установить в качестве контента элемента с классом testParagraph следующий параграф: "This is a paragraph":`);

let testParagraphClassElement = document.querySelector('.testParagraph');
if (testParagraphClassElement = 'null') {
    console.log(testParagraphClassElement, `\u{26D4} Элемента с таким классом не существует, попробуем найти его по id`);
}

const testParagraphElement = document.querySelector('#testParagraph');

if (testParagraphElement) {
    console.log(`\u{2705} Есть элемент с таким id - дальше продолжеаем с ним`, testParagraphElement);
}

testParagraphElement.innerHTML = `This is a paragraph`;
console.log(testParagraphElement.textContent);
console.log(testParagraphElement.outerHTML);

// Получить элементы, вложенные в элемент с классом main-header и вывести их в консоль.
console.log(`\u{1F6A9} Получить элементы, вложенные в элемент с классом main-header и вывести их в консоль.:`);

const mainHeaderElement = document.querySelector('.main-header');
console.log(mainHeaderElement);

const mainHeaderElementChildren = mainHeaderElement.children;
console.log(mainHeaderElementChildren);

// Каждому элементу присвоить новый класс nav-item.
console.log(`\u{1F6A9} Каждому элементу присвоить новый класс nav-item:`);

for (const newClass of mainHeaderElementChildren ) {
    newClass.classList.add('nav-item');
    console.log(newClass);
}

// Найти все элементы с классом section-title. Удалить этот класс у данных элементов.
console.log(`\u{1F6A9} Найти все элементы с классом section-title. Удалить этот класс у данных элементов:`);

const sectionTitleElements = document.querySelectorAll('.section-title');
console.log(sectionTitleElements);

console.log(`\u{1F6A9} Результат выведен в консоль:`);
for (const item of sectionTitleElements) {
    item.classList.remove('section-title');
    console.log(item);
}

// ------< TASK#1 >------ЗАВЕРШЕН------------------------
console.log ('Task #1 < HW8 - searching-dom-tasks > finished!:) \u{1F60E} \u{1F3C1}')


